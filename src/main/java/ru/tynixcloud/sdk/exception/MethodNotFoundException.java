package ru.tynixcloud.sdk.exception;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class MethodNotFoundException extends Exception {

    @Override
    public String getLocalizedMessage() {
        return "Метод не найден!";
    }

    @Override
    public String getMessage() {
        return "Im sorry, used method not found.";
    }
}
